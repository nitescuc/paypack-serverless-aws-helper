"use strict"

const uuid = require('uuid');

class ServerlessPluginHelper {
    constructor(serverless, options) {
        this.serverless = serverless;
        this.options = options;
        this.provider = this.serverless.getProvider('aws');
    }

    async eachEvent(cb) {
        const functions = this.serverless.service.functions || {};
        for (let key of Object.keys(functions)) {
            this.serverless.cli.log(`Scanning function ${key}`);
            const ff = functions[key];
            for (let event of (ff.events || [])) {
                await cb(ff, event);
            }
        }
    }
    
    async lambdaAddInvokePermission(options) {
        const { FunctionName, Principal, SourceArn } = options;
        //
        let policy = null;
        try {
          let resp = await this.provider.request('Lambda', 'getPolicy', {
            FunctionName
          });
          policy = JSON.parse(resp.Policy);
        } catch(e) {
          if (e.statusCode !== 404) console.log('Cannot describe Policy for Lambda', e);
        }
        //
        if (!policy || !policy.Statement.some((statement) => {
          return statement.Principal && statement.Principal.Service === Principal;
        })) {
          console.log(`Adding permission: Lambda=${FunctionName}; Principal=${Principal}; SourceArn=${SourceArn}`);
          let resp = await this.provider.request('Lambda', 'addPermission', {
            FunctionName,
            Action: "lambda:InvokeFunction",
            Principal,
            StatementId: `lambda-${uuid()}`,
            SourceArn
          });
        } else {
            console.log(`Policy already exists: Lambda=${FunctionName}; Principal=${Principal}; SourceArn=${SourceArn}`);
        }
    }
}

module.exports = ServerlessPluginHelper;